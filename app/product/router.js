const os = require('os');
// (1) import router dari express
const router = require('express').Router();
const multer = require('multer');

// (2) import product controller
const productController = require('./controller'); 
// (3) pasangkan route endpoint dengan method `store`
// router.post('/products', productController.store);
router.get('/products', productController.index);
// (2) gunakan multer().none() sebagai middleware
router.post('/products', multer({dest: os.tmpdir()}).single('image'), productController.store);
// update product dengan gambar
router.put('/products/:id', multer({dest: os.tmpdir()}).single('image'), productController.update);
router.delete('/products/:id', productController.destroy);
// (4) export router
module.exports = router;