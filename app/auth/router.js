// (1) import package yang diperlukan
const router = require('express').Router(); 
const multer = require('multer');
const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;


const controller = require('./controller'); // (3) buat endpoint untuk register user baru
// (2) import auth/controller.js
passport.use(new LocalStrategy({usernameField: 'email'}, controller.localStrategy));
router.post('/register', multer().none(), controller.register);
router.post('/login', multer().none(), controller.login);
router.get('/users', multer().none(), controller.index);
router.get('/me', controller.me);
router.post('/logout', controller.logout);

// (4) export router
module.exports = router;